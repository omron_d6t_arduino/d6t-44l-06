/*
 LiquidCrystal Library - display() and noDisplay()

 Demonstrates the use a 16x2 LCD display and serial port to display data read
 from sensor D6T
 
 The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 The example code for LCD can found here
 http://www.arduino.cc/en/Tutorial/LiquidCrystalDisplay

*/

// include the library code:
#include <LiquidCrystal.h>
#include <Wire.h>

//#define DEBUG
#define  D6T_addr 0x0A
#define  D6T_cmd 0x4C

char data[64] = {0};

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  Serial.begin(115200); // start serial communication at 115200bps
  while (!Serial);

  Wire.begin(); // join i2c bus (address optional for master)

  delay(1000);
}

unsigned char calc_crc(unsigned char data)
{
  int index;
  unsigned char temp;
  
  for (index = 0; index < 8; index++)
  {
    temp = data;
    data <<= 1;
    if (temp & 0x80) data ^= 0x07;
  }
  return data;
}

int CheckPEC(char buf, int pPEC)
{
  int i;
  unsigned char crc;
  
  crc = calc_crc( 0x15 );
  for (i = 0; i < pPEC; i++)
  {
    crc = calc_crc( data[i] ^ crc );
  }
  return (crc == data[pPEC]);
}

int8_t d6t_getData(byte *data) {
  uint8_t i = 0;
#ifdef DEBUG
  Serial.println("d6t_getData");
#endif

  Wire.beginTransmission(byte(D6T_addr));
  Wire.write(byte(D6T_cmd));
  Wire.endTransmission();

  delay(100);

  int byteCount = Wire.requestFrom(D6T_addr, 35);
#ifdef DEBUG
  Serial.print("d6t_getData, byteCount:");
  Serial.println(byteCount);
#endif
  if (byteCount > 0) {
    while(Wire.available())
    {
      data[i++] = Wire.read();
    }
  }

#ifdef DEBUG
  Serial.print("d6t_getData, i:");
  Serial.println(i);
#endif
  if (i < 35 || CheckPEC(data, 34) == 0) {
    return -1;
  }

  return 0;
}

void loop() {
  uint8_t i;
  int16_t temp[16] = {0};
  int16_t sum = 0;

  lcd.clear();
  // Print a message to the LCD.
  lcd.print("Omron, D6T avg temp:");
  // set the cursor to column 0, line 1
  lcd.setCursor(0, 1);
  Serial.print("Test: ");

  if (!d6t_getData(data)) {
    Serial.print("D6T data: ");
    for (i = 0; i < 17; i++) {
      temp[i] = 256 * data[2 * i + 1] + data[2 * i];
      sum += temp[i];
      Serial.print(temp[i]);
      Serial.print(", ");
    }
    Serial.println(data[2 * i]);
    lcd.print(sum / 16);
  } else {
    Serial.println("Data error!");
    lcd.print("Data error!");
  }

  delay(2000); // wait a bit since people have to read the output :)
}
